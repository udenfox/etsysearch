package xyz.spacefox.etsysearch.app;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import xyz.spacefox.etsysearch.app.api.model.ListingItem;
import xyz.spacefox.etsysearch.app.utils.Consts;
import xyz.spacefox.etsysearch.app.utils.Database;

/**
 * Detailed View Activity: shows item detailed view including picture, price, title and description.
 *
 * User can save displayed item to a local database by pressing floating action button.
 */
public class DetailedViewActivity extends AppCompatActivity {

    // Activity Toolbar
    private Toolbar mToolbar;

    // Floating action button to save item to the database
    private FloatingActionButton mFab;

    // Views to represent item content
    private TextView mItemTitle;
    private TextView mItemPrice;
    private TextView mItemDescription;
    private ImageView mItemPicture;

    // Item to display
    private ListingItem mItem;

    // Database helper instance
    private Database mDb;

    // Displays is item cached in database or not
    private Boolean isCached = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_view);
        initToolbar();

        // Getting item to display from intent
        mItem = (ListingItem) getIntent().getSerializableExtra(Consts.EXTRA_ITEM);

        // Init database and check if current item cached
        mDb = new Database(DetailedViewActivity.this);
        mDb.open();
        isCached = mDb.isItemCached(mItem);

        // Init and configure Floating action button
        mFab = (FloatingActionButton) findViewById(R.id.save_fab);
        mFab.setOnClickListener(view -> {

            // Add item to database if not cached and delete in other case
            if(!isCached) {
                isCached = true;
                mFab.setImageDrawable(ContextCompat.getDrawable(DetailedViewActivity.this,
                        R.drawable.ic_star_white_24dp));
                mDb.addItem(mItem);
                Snackbar.make(view, getString(R.string.item_added_snack), Snackbar.LENGTH_SHORT)
                        .show();
            } else {
                isCached = false;
                mFab.setImageDrawable(ContextCompat.getDrawable(DetailedViewActivity.this,
                        R.drawable.ic_star_outline_white_24dp));
                mDb.deleteItem(mItem);
                Snackbar.make(view, getString(R.string.item_deleted_snack), Snackbar.LENGTH_SHORT)
                        .show();
            }

        });

        // Set proper floating action button icon depends on cache status
        if(isCached) {
            mFab.setImageDrawable(ContextCompat.getDrawable(DetailedViewActivity.this,
                    R.drawable.ic_star_white_24dp));
        } else {
            mFab.setImageDrawable(ContextCompat.getDrawable(DetailedViewActivity.this,
                    R.drawable.ic_star_outline_white_24dp));
        }

        // Locate item views
        mItemTitle = (TextView) findViewById(R.id.item_title);
        mItemPrice = (TextView) findViewById(R.id.item_price);
        mItemDescription = (TextView) findViewById(R.id.item_description);
        mItemPicture = (ImageView) findViewById(R.id.item_picture);

        // Setting content to item views
        mItemTitle.setText(Html.fromHtml(mItem.getTitle()));
        String descriptionHtml = mItem.getDescription().replace("\n", "<br>");
        mItemDescription.setText(Html.fromHtml(descriptionHtml));
        mItemPrice.setText(String.format("%.02f", mItem.getPrice())+" "+mItem.getCurrency());
        Picasso.with(DetailedViewActivity.this).load(mItem.getImage().getUrl()).into(mItemPicture);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Close database connection
        mDb.close();
    }

    /** Initializes the toolbar. */
    private void initToolbar(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
    }

}
