package xyz.spacefox.etsysearch.app.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Class that represents JSON API response.
 */
public class ApiResponse<T> {

    @SerializedName("count")
    private Integer mCount;

    @SerializedName("results")
    private List<T> mResults;

    public Integer getCount() {
        return mCount;
    }

    public List<T> getResults() {
        return mResults;
    }
}
