package xyz.spacefox.etsysearch.app.api.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Class that represents Listing object from JSON API response.
 */
public class ListingItem implements Serializable {

    @SerializedName("listing_id")
    private int mId;

    @SerializedName("category_id")
    private int mCategoryId;

    @SerializedName("title")
    private String mTitle;

    @SerializedName("description")
    private String mDescription;

    @SerializedName("price")
    private float mPrice;

    @SerializedName("currency_code")
    private String mCurrency;

    @SerializedName("MainImage")
    private ItemImage mImage;

    private static final long serialVersionUID = 1L;

    public int getId() {
        return mId;
    }

    public int getCategoryId() {
        return mCategoryId;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getDescription() {
        return mDescription;
    }

    public float getPrice() {
        return mPrice;
    }

    public String getCurrency() {
        return mCurrency;
    }

    public ItemImage getImage() {
        return mImage;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public void setCategoryId(int categoryId) {
        this.mCategoryId = categoryId;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public void setDescription(String description) {
        this.mDescription = description;
    }

    public void setPrice(float price) {
        this.mPrice = price;
    }

    public void setCurrency(String currency) {
        this.mCurrency = currency;
    }

    public void setImage(ItemImage image) {
        this.mImage = image;
    }

    @Override
    public int hashCode() {
        return getId();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null)
            return false;
        if (getClass() != o.getClass())
            return false;
        ListingItem other = (ListingItem) o;
        if (mId != other.getId())
            return false;
        return true;
    }
}
