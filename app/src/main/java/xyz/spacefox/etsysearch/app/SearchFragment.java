package xyz.spacefox.etsysearch.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import xyz.spacefox.etsysearch.app.api.RestClient;
import xyz.spacefox.etsysearch.app.api.model.Category;
import xyz.spacefox.etsysearch.app.ui.CategoryCursorAdapter;
import xyz.spacefox.etsysearch.app.utils.Consts;
import xyz.spacefox.etsysearch.app.utils.Database;

/**
 * Fragment that displays search interface.
 *
 * This Fragment displays a search field, category chooser and "Search" button.
 */
public class SearchFragment extends Fragment{

    // SearchView to insert search query in
    private SearchView mSearchView;

    // Spinner to choose category
    private Spinner mSpinner;

    // "Search" button
    private Button mSearchBtn;

    // Cursor adapter for category chooser
    private CategoryCursorAdapter mCategoryAdapter;

    // Database helper reference
    private Database mDb;

    // Selected category to search in
    private String mSelectedCategory;

    /**
     * Create a new instance of NewsFeedFragment.
     */
    public static SearchFragment newInstance() {
        SearchFragment fragment = new SearchFragment();
        return fragment;
    }

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialize database and cursor adapter
        mDb = new Database(getActivity());
        mCategoryAdapter = new CategoryCursorAdapter(getActivity(), null, true);

        // Open our database
        mDb.open();

        // Load categories
        loadCategory();

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_search, container, false);

        // Locate and set up search view
        mSearchView = (SearchView) v.findViewById(R.id.main_search_view);
        mSearchView.setIconified(false);
        mSearchView.onActionViewExpanded();
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                doSearch();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        // Locate and set up search button
        mSearchBtn = (Button) v.findViewById(R.id.btn_search);
        mSearchBtn.setOnClickListener(v1 -> doSearch());

        // Locate and set up adapter, listener for a spinner
        mSpinner = (Spinner) v.findViewById(R.id.category_spinner);
        mSpinner.setAdapter(mCategoryAdapter);
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                //Set selected category to chosen one
                mSelectedCategory = mCategoryAdapter.getItem(position).getCategoryNameId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        return v;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mDb.close();
    }

    /** Loads the category list using API */
    private void loadCategory() {

        // Try to load cached data first
        mCategoryAdapter.swapCursor(mDb.getAllCategorys());

        // Try to load entries from API
        RestClient.getInstance().getApiService().getCategory().
                subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(categoryApiResponse -> categoryApiResponse.getResults())
                .subscribe(new Subscriber<List<Category>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        //Show toast with error message
                        Toast.makeText(getActivity(), getString(R.string.error_no_connection), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(List<Category> categoryList) {
                        //Delete all categories from database to avoid invalidating
                        mDb.deleteAllCategories();

                        // Add loaded category and notify loader to load data
                        mDb.addCategoryList(categoryList);
                        mCategoryAdapter.swapCursor(mDb.getAllCategorys());
                    }
                });
    }

    private void doSearch(){
        if(mSearchView.getQuery().toString().trim().length() > 0){
            Intent searchIntent = new Intent(getActivity(), SearchResultsActivity.class);
            searchIntent.putExtra(Consts.EXTRA_SEARCH_CATEGORY, mSelectedCategory);
            searchIntent.putExtra(Consts.EXTRA_SEARCH_QUERY, mSearchView.getQuery().toString().trim());
            startActivity(searchIntent);
        }
    }

}