package xyz.spacefox.etsysearch.app;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.inputmethod.InputMethodManager;

import java.sql.SQLOutput;

import xyz.spacefox.etsysearch.app.ui.ViewPagerAdapter;
import xyz.spacefox.etsysearch.app.utils.Consts;

/**
 * Main Activity: contains two tabs for search items and show already saved items
 *
 * Search items tab contains two fields: search field and category spinner. After entering query and
 * choosing category to search in user can tap on "Search" button or use keyboard "search" button to
 * start searching.
 *
 * Saved items tab displays items that had been saved by a user to local database. Clicking on item
 * opens detailed view activity.
 * User can delete item from saved list by long clicking on it and tap on "Delete" menu item.
 */
public class MainActivity extends AppCompatActivity {

    // Activity toolbar
    private Toolbar mToolbar;

    // Main ViewPager
    private ViewPager mViewPager;

    // TabLayout to host two tabs
    private TabLayout mTabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Init toolbar
        initToolbar();

        // Locate and init viewpager
        mViewPager = (ViewPager) findViewById(R.id.main_viewpager);
        initViewPager();

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == Consts.SAVED_TAB_POSITION) {
                    final InputMethodManager imm = (InputMethodManager)getSystemService(
                            Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(mViewPager.getWindowToken(), 0);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        // Locate and setup TabLayout
        mTabLayout = (TabLayout) findViewById(R.id.main_tabs);
        mTabLayout.setupWithViewPager(mViewPager);

    }

    /** Initializes the toolbar. */
    private void initToolbar(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getString(R.string.app_name));
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(mToolbar);
    }

    /** Initializes the ViewPager. */
    private void initViewPager(){
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(SearchFragment.newInstance(), getString(R.string.tab_search));
        adapter.addFragment(SavedItemsFragment.newInstance(), getString(R.string.tab_saved));
        mViewPager.setAdapter(adapter);
    }


}
