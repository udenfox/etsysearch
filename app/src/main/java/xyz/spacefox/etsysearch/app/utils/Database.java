package xyz.spacefox.etsysearch.app.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import xyz.spacefox.etsysearch.app.api.model.Category;
import xyz.spacefox.etsysearch.app.api.model.ListingItem;

import java.util.List;

/**
 * Class to simplify interaction with the database. Contains specific methods to interact
 * with databese depends on project's needs.
 */
public class Database {

    private final Context mCtx;

    // Custom DbHelper.
    private DBHelper mDBHelper;

    // Database itself
    private SQLiteDatabase mDB;

    /**
     * Constructor of database object.
     * @param ctx Context for database.
     */
    public Database(Context ctx) {
        mCtx = ctx;
    }

    /**
     * Opens a connection to database
     */
    public void open() {
        mDBHelper = new DBHelper(mCtx, Consts.DB_NAME, null, Consts.DB_VERSION);
        mDB = mDBHelper.getWritableDatabase();
    }

    /**
     * Closes the connection to database.
     */
    public void close() {
        if (mDBHelper!=null) mDBHelper.close();
    }

    /**
     * Returns cursor with all categorys from database
     */
    public Cursor getAllCategorys() {
        return mDB.query(Consts.DB_TABLE_CATEGORY, null, null, null, null, null, Consts.DB_COL_ID_PRIMARY );
    }

    /**
     * Returns cursor with all items from database
     */
    public Cursor getAllItems() {
        return mDB.query(Consts.DB_TABLE_ITEMS, null, null, null, null, null, Consts.DB_COL_ID_PRIMARY +" DESC");
    }

    /**
     * Add category item to the database
     */
    public void addCategoryItem(Category item) {
        ContentValues cv = new ContentValues();
        cv.put(Consts.DB_COL_ID_PRIMARY, item.getCategoryId());
        cv.put(Consts.DB_CATEGORY_COL_NAME, item.getCategoryShortName());
        cv.put(Consts.DB_CATEGORY_COL_NAME_ID, item.getCategoryNameId());
        mDB.insert(Consts.DB_TABLE_CATEGORY, null, cv);
    }

    /**
     * Add item to the database
     */
    public void addItem(ListingItem item) {
        ContentValues cv = new ContentValues();
        cv.put(Consts.DB_COL_ID_PRIMARY, item.getId());
        cv.put(Consts.DB_ITEM_COL_CATEGORY, item.getCategoryId());
        cv.put(Consts.DB_ITEM_COL_TITLE, item.getTitle());
        cv.put(Consts.DB_ITEM_COL_DESCRIPTION, item.getDescription());
        cv.put(Consts.DB_ITEM_COL_PRICE, item.getPrice());
        cv.put(Consts.DB_ITEM_COL_CURRENCY, item.getCurrency());
        cv.put(Consts.DB_ITEM_COL_IMAGE_URL, item.getImage().getUrl());

        mDB.insert(Consts.DB_TABLE_ITEMS, null, cv);
    }

    /**
     * Delete specified item from database.
     * @param item Item to delete
     */
    public void deleteItem(ListingItem item) {
        mDB.delete(Consts.DB_TABLE_ITEMS, Consts.DB_COL_ID_PRIMARY + "=" + item.getId(), null);
    }

    /**
     * Check if database already have specified item
     * @param item Item to check
     */
    public boolean isItemCached(ListingItem item){
        Cursor cursor = mDB.query(Consts.DB_TABLE_ITEMS, null,
                Consts.DB_COL_ID_PRIMARY + "=" + item.getId(), null, null, null, null);
        boolean isCached = cursor.getCount() > 0;
        cursor.close();
        return isCached;
    }

    /**
     * Add list of category items to the database
     */
    public void addCategoryList(List<Category> items){
        if (items.size() != 0) {
            for (int i = items.size()-1; i>=0; i--){
                addCategoryItem(items.get(i));
            }
        }

    }

    /**
     * Delete all categories from database.
     */
    public void deleteAllCategories() {
        mDB.delete(Consts.DB_TABLE_CATEGORY, Consts.DB_COL_ID_PRIMARY+" NOT IN (0)", null);
    }

    /**
     * Subclass of {@link android.database.sqlite.SQLiteOpenHelper} which provides custom database helper.
     */
    private class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory,
                        int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(Consts.DB_CREATE_ITEM_TABLE);
            db.execSQL(Consts.DB_CREATE_CATEGORY_TABLE);
            db.execSQL(Consts.DB_ADD_EMPTY_CATEGORY);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(Consts.DB_DROP_CATEGORY);
            db.execSQL(Consts.DB_DROP_ITEMS);
            onCreate(db);
        }
    }

}
