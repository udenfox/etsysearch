package xyz.spacefox.etsysearch.app.ui;

import android.content.Context;
import android.database.Cursor;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import xyz.spacefox.etsysearch.app.R;
import xyz.spacefox.etsysearch.app.api.model.ItemImage;
import xyz.spacefox.etsysearch.app.api.model.ListingItem;
import xyz.spacefox.etsysearch.app.utils.Consts;

/**
 * Subclass of {@link android.widget.CursorAdapter} which provides cursor adapter for custom list item and dataset.
 */
public class ItemsCursorAdapter extends CursorAdapter {

    private LayoutInflater cursorInflater;

    public ItemsCursorAdapter(Context context, Cursor cursor, boolean autoRequery) {
        super(context, cursor, autoRequery);
        cursorInflater = (LayoutInflater) context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return cursorInflater.inflate(R.layout.grid_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        // Locating item view elements
        ImageView itemImage = (ImageView) view.findViewById(R.id.item_image_thumb);
        TextView itemName = (TextView) view.findViewById(R.id.item_name);

        // Setting content to the view elements
        String title = cursor.getString(cursor.getColumnIndex(Consts.DB_ITEM_COL_TITLE));
        String imageUrl = cursor.getString(cursor.getColumnIndex(Consts.DB_ITEM_COL_IMAGE_URL));
        if (title != null){
            itemName.setText(Html.fromHtml(title));
        }
        if (imageUrl != null){
            Picasso.with(context).load(imageUrl).into(itemImage);
        }

    }

    /**
     * Returns an {@link xyz.spacefox.etsysearch.app.api.model.ListingItem} object collected
     * from database at specified position.
     * @param position Position of item to get
     */
    public ListingItem getItem(int position){
        Cursor c = getCursor();
        ListingItem item = new ListingItem();
        ItemImage image = new ItemImage();
        if(c.moveToPosition(position)){
            image.setUrl(c.getString(c.getColumnIndex(Consts.DB_ITEM_COL_IMAGE_URL)));
            item.setImage(image);
            item.setId(c.getInt(c.getColumnIndex(Consts.DB_COL_ID_PRIMARY)));
            item.setCategoryId(c.getInt(c.getColumnIndex(Consts.DB_ITEM_COL_CATEGORY)));
            item.setTitle(c.getString(c.getColumnIndex(Consts.DB_ITEM_COL_TITLE)));
            item.setDescription(c.getString(c.getColumnIndex(Consts.DB_ITEM_COL_DESCRIPTION)));
            item.setPrice(c.getFloat(c.getColumnIndex(Consts.DB_ITEM_COL_PRICE)));
            item.setCurrency(c.getString(c.getColumnIndex(Consts.DB_ITEM_COL_CURRENCY)));
        }

        return item;

    }
}
