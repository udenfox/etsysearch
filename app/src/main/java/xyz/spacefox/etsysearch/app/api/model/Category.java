package xyz.spacefox.etsysearch.app.api.model;

import com.google.gson.annotations.SerializedName;

/**
 * Class that represents Category object from JSON API response.
 */
public class Category {

    @SerializedName("category_id")
    private int mCategoryId;

    @SerializedName("category_name")
    private String mCategoryNameId;

    @SerializedName("short_name")
    private String mCategoryShortName;

    public Category() {
    }

    /**
     * Constructor to create Category object that represents all category search
     */
    public Category(boolean isAllCategory) {
        if(isAllCategory){
            this.mCategoryShortName = "All categories";
        }
    }

    public Integer getCategoryId() {
        return mCategoryId;
    }

    public String getCategoryShortName() {
        return mCategoryShortName;
    }

    public String getCategoryNameId() {
        return mCategoryNameId;
    }

    public void setCategoryId(Integer mCategoryId) {
        this.mCategoryId = mCategoryId;
    }

    public void setCategoryShortName(String categoryShortName) {
        this.mCategoryShortName = categoryShortName;
    }

    public void setCategoryNameId(String categoryNameId) {
        this.mCategoryNameId = categoryNameId;
    }

    @Override
    public int hashCode() {
        return getCategoryId();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null)
            return false;
        if (getClass() != o.getClass())
            return false;
        Category other = (Category) o;
        if (mCategoryId != other.getCategoryId())
            return false;
        return true;
    }
}
