package xyz.spacefox.etsysearch.app.utils;

/**
 * Class that contains all project constants
 */
public class Consts {

    public final static String API_URL = "https://openapi.etsy.com/v2";
    public final static String API_KEY = "l6pdqjuf7hdf97h1yvzadfce";
    public final static int ITEMS_PER_REQUEST = 25;
    public final static int SAVED_TAB_POSITION = 1;

    // Intent extras and tags
    public final static String EXTRA_SEARCH_CATEGORY = "SEARCH_CATEGORY";
    public final static String EXTRA_SEARCH_QUERY = "SEARCH_QUERY";
    public final static String EXTRA_ITEM = "SELECTED_ITEM";
    public final static String INSTANCE_TAG_ITEMS_COUNT = "ITEMS_COUNT";
    public final static String INSTANCE_TAG_ITEMS_OFFSET = "ITEMS_OFFSET";

    // Database
    public final static int CATEGORY_LOADER_ID = 0;
    public final static int ITEMS_LOADER_ID = 1;
    public final static String DB_NAME = "es_db";
    public final static String DB_COL_ID_PRIMARY = "_id";
    public final static int DB_VERSION = 3;

    // Categories table
    public final static String DB_TABLE_CATEGORY = "es_db_category";
    public final static String DB_CATEGORY_COL_NAME = "category_name";
    public final static String DB_CATEGORY_COL_NAME_ID = "category_name_id";

    // Items table
    public final static String DB_TABLE_ITEMS = "es_db_items";
    public final static String DB_ITEM_COL_CATEGORY = "item_category";
    public final static String DB_ITEM_COL_TITLE = "item_title";
    public final static String DB_ITEM_COL_DESCRIPTION = "item_description";
    public final static String DB_ITEM_COL_PRICE = "item_price";
    public final static String DB_ITEM_COL_CURRENCY = "item_currency";
    public final static String DB_ITEM_COL_IMAGE_URL = "item_image_url";



    // Queries
    public final static String DB_CREATE_CATEGORY_TABLE =
            "create table " + DB_TABLE_CATEGORY + "( " +
                    DB_COL_ID_PRIMARY + " integer primary key, " +
                    DB_CATEGORY_COL_NAME + " text, "+
                    DB_CATEGORY_COL_NAME_ID + " text, "+
                    " UNIQUE ( "+DB_COL_ID_PRIMARY+" ) ON CONFLICT IGNORE"+
                    ");";

    public final static String DB_CREATE_ITEM_TABLE =
            "create table " + DB_TABLE_ITEMS + "( " +
                    DB_COL_ID_PRIMARY + " integer primary key, " +
                    DB_ITEM_COL_CATEGORY + " integer, "+
                    DB_ITEM_COL_TITLE + " text, "+
                    DB_ITEM_COL_DESCRIPTION + " text, "+
                    DB_ITEM_COL_PRICE + " real, "+
                    DB_ITEM_COL_CURRENCY + " text, "+
                    DB_ITEM_COL_IMAGE_URL + " text, "+
                    " UNIQUE ( "+DB_COL_ID_PRIMARY+" ) ON CONFLICT IGNORE"+
                    ");";

    public static final String DB_ADD_EMPTY_CATEGORY =
            "INSERT INTO " + DB_TABLE_CATEGORY +
                    "( " + DB_COL_ID_PRIMARY + ", " + DB_CATEGORY_COL_NAME + ") " +
                    "VALUES ( 0, 'All categories' );";

    public static final String DB_DROP_CATEGORY =
            "DROP TABLE IF EXISTS " + DB_TABLE_CATEGORY;

    public static final String DB_DROP_ITEMS =
            "DROP TABLE IF EXISTS " + DB_TABLE_ITEMS;


}
