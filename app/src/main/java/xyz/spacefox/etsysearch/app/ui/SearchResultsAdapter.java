package xyz.spacefox.etsysearch.app.ui;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import xyz.spacefox.etsysearch.app.R;
import xyz.spacefox.etsysearch.app.api.model.ListingItem;

/**
 * Subclass of {@link android.widget.BaseAdapter} which provides array adapter for custom list
 * item and dataset.
 */
public class SearchResultsAdapter extends BaseAdapter {

    private Context mCtx;

    // Inflater to inflate item layout
    private LayoutInflater mLayoutInflater;

    // Adapter items
    private List<ListingItem> mItems;

    public SearchResultsAdapter(Context context, List<ListingItem> items) {
        this.mCtx = context;
        this.mItems = items;
        mLayoutInflater = (LayoutInflater) mCtx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public ListingItem getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View view = convertView;

        // Locating view elements
        if (view == null) {
            view = mLayoutInflater.inflate(R.layout.grid_item, null, true);
            holder = new ViewHolder();
            holder.itemName = (TextView) view.findViewById(R.id.item_name);
            holder.itemPicture = (ImageView) view.findViewById(R.id.item_image_thumb);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        // Setting content to view elements
        String title = getItem(position).getTitle();
        if(title != null){
            holder.itemName.setText(Html.fromHtml(title));
        }
        if (getItem(position).getImage() != null){
            Picasso.with(mCtx).load(getItem(position).getImage().getUrl()).into(holder.itemPicture);
        }

        return view;
    }

    /**
     * Viewholder class to hold grid item view
     */
    private static class ViewHolder {
        public ImageView itemPicture;
        public TextView itemName;
    }

}
