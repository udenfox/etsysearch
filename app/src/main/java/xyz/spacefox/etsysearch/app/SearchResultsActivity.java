package xyz.spacefox.etsysearch.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import xyz.spacefox.etsysearch.app.api.RestClient;
import xyz.spacefox.etsysearch.app.api.model.ApiResponse;
import xyz.spacefox.etsysearch.app.api.model.ListingItem;
import xyz.spacefox.etsysearch.app.ui.SearchResultsAdapter;
import xyz.spacefox.etsysearch.app.utils.Consts;

/**
 * Search Results Activity: displays items that founded by user query in specified category.
 *
 * User can refresh results using Swipe-to-refresh functionality.
 * If availible, more data loaded automatically when gridview scrolled to end.
 *
 * Clicking on item opens item detailed view.
 */
public class SearchResultsActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    // Activity toolbar
    private Toolbar mToolbar;

    // Gridview to show results in
    private GridView mResultsGrid;

    // Adapter to fill GridView by data
    private SearchResultsAdapter mResultsAdapter;

    // View that represents empty view for grid
    private TextView mEmptyView;

    // Swipe-refresh layout
    private SwipeRefreshLayout mRefreshLayout;

    // Category to search in
    private String mSearchCategory;

    // Query to search by
    private String mSearchQuery;

    // List that contains founded items
    private List<ListingItem> mResultsItems;

    // Total items found
    private int mResultsCount;

    // Offset to implement automatic pagination
    private int mOffset = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results);

        // Getting search data from intent
        mSearchCategory = getIntent().getStringExtra(Consts.EXTRA_SEARCH_CATEGORY);
        mSearchQuery = getIntent().getStringExtra(Consts.EXTRA_SEARCH_QUERY);

        // Init the toolbar
        initToolbar();

        // Init and configure Swipe-refresh layout
        mRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        mRefreshLayout.setOnRefreshListener(this);
        mRefreshLayout.setColorSchemeResources(R.color.primary_dark);

        // Try to load search results list from a saved state
        try {
            mResultsItems  = (ArrayList<ListingItem>) getLastCustomNonConfigurationInstance();
        } catch (NullPointerException e) {
            mResultsItems = new ArrayList<>();
        }

        if(mResultsItems==null){
            mResultsItems = new ArrayList<>();
        }

        // Init adapter with data
        mResultsAdapter = new SearchResultsAdapter(SearchResultsActivity.this, mResultsItems);

        // Locate grid's empty view.
        mEmptyView = (TextView) findViewById(R.id.empty_text);

        // Init and configure GridView's listeners
        mResultsGrid = (GridView) findViewById(R.id.search_results_grid);
        mResultsGrid.setEmptyView(mEmptyView);
        mResultsGrid.setAdapter(mResultsAdapter);
        mResultsGrid.setOnItemClickListener((parent, view, position, id) -> {
            Intent detailIntent = new Intent(SearchResultsActivity.this,
                    DetailedViewActivity.class);
            detailIntent.putExtra(Consts.EXTRA_ITEM, mResultsAdapter.getItem(position));
            startActivity(detailIntent);
        });

        // Init Grid Scroll Listener to implement automatic pagination
        mResultsGrid.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                // Load data if available and if grid scrolled to the end
                boolean isEnd = firstVisibleItem + visibleItemCount >= totalItemCount &&
                        totalItemCount > 0 && mResultsCount > totalItemCount &&
                        !mRefreshLayout.isRefreshing();
                if (isEnd) {
                    mOffset += Consts.ITEMS_PER_REQUEST;
                    loadData(false, mOffset);
                }
            }
        });

        // Init data loading from API if activity started first time
        if (savedInstanceState==null || mResultsItems.size() == 0) {
            loadData(true, mOffset);
        }

    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        // Save loaded items list to refill grid after screen rotate
        return mResultsItems;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save Results count and offset to proper data loading after screen rotate
        outState.putInt(Consts.INSTANCE_TAG_ITEMS_COUNT, mResultsCount);
        outState.putInt(Consts.INSTANCE_TAG_ITEMS_OFFSET, mOffset);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // Try to restore count and offset after activity loaded again
        if(savedInstanceState != null){
            mResultsCount = savedInstanceState.getInt(Consts.INSTANCE_TAG_ITEMS_COUNT);
            mOffset =  savedInstanceState.getInt(Consts.INSTANCE_TAG_ITEMS_OFFSET);
        }

    }

    /**
     * Loads search results from API.
     * @param refresh true if total data refresh is needed.
     * @param offset Offset to load proper data page.
     */
    private void loadData(final boolean refresh, int offset) {

        // Show refresh indicator when data loading and change empy view text
        setRefreshing(true);
        mEmptyView.setText(getString(R.string.loading_empty));

        RestClient.getInstance().getApiService().getItems(mSearchQuery, mSearchCategory, mOffset)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ApiResponse<ListingItem>>() {
                    @Override
                    public void onCompleted() {
                        setRefreshing(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        //Inform user about error.
                        Toast.makeText(SearchResultsActivity.this, getString(R.string.error_no_connection),
                                Toast.LENGTH_SHORT).show();
                        mEmptyView.setText(getString(R.string.error_empty));
                    }

                    @Override
                    public void onNext(ApiResponse<ListingItem> listingItemApiResponse) {
                        // If "refresh" is true - clear previously loaded data first
                        if (listingItemApiResponse.getCount() > 0){
                            if (refresh){
                                mResultsItems.clear();
                            }

                            // Set counters and loaded items to a list.
                            mResultsCount = listingItemApiResponse.getCount();
                            mResultsItems.addAll(listingItemApiResponse.getResults());

                            // Update adapter to show actual items
                            mResultsAdapter.notifyDataSetChanged();
                        } else {
                            mEmptyView.setText(getString(R.string.no_items_empty));
                        }

                    }
                });

    }

    /** Initializes the toolbar. */
    private void initToolbar(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(String.format(getString(R.string.search_results_toolbar_text), mSearchQuery));
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(mToolbar);
    }

    /** Sets refreshing state to Swipe-refresh layout in proper way */
    private void setRefreshing(final boolean state){
        if (mRefreshLayout != null){
            mRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mRefreshLayout.setRefreshing(state);
                }
            });
        }
    }

    @Override
    public void onRefresh() {

        // Load results on user used swipe-to-refresh
        mOffset = 0;
        loadData(true, mOffset);
    }
}
