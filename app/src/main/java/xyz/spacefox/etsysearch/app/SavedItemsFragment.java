package xyz.spacefox.etsysearch.app;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.PopupMenu;
import android.widget.TextView;

import xyz.spacefox.etsysearch.app.ui.ItemsCursorAdapter;
import xyz.spacefox.etsysearch.app.utils.Consts;
import xyz.spacefox.etsysearch.app.utils.Database;

/**
 * Fragment that displays saved items.
 *
 * This Fragment displays a grid of items that user saved to a database.
 * Clicking on itemopens detailed view activity.
 * User can delete item from saved list by long clicking on it and tap on "Delete" menu item.
 *
 * Implements LoaderCallbacks to get connection with database.
 */
public class SavedItemsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    // Database helper instance
    private Database mDb;

    // GridView to show saved items in it
    private GridView mItemsGrid;

    // TextView that represents empty view of grid
    private TextView mEmptyText;

    // Cursor adapter to populate items to a grid
    private ItemsCursorAdapter mItemsAdapter;

    /**
     * Create a new instance of NewsFeedFragment.
     */
    public static SavedItemsFragment newInstance() {
        SavedItemsFragment fragment = new SavedItemsFragment();
        return fragment;
    }

    public SavedItemsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialize database and cursor adapter
        mDb = new Database(getActivity());
        mItemsAdapter = new ItemsCursorAdapter(getActivity(), null, true);

        // Open our database
        mDb.open();

        // Initialize cursor loader
        getLoaderManager().initLoader(Consts.ITEMS_LOADER_ID, null, this);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_saved_items, container, false);

        // Locate View elements
        mEmptyText = (TextView) v.findViewById(R.id.empty_text);
        mItemsGrid = (GridView) v.findViewById(R.id.saved_grid);

        // Init and setup gridview's listeners and adapter
        mItemsGrid.setEmptyView(mEmptyText);
        mItemsGrid.setAdapter(mItemsAdapter);
        mItemsGrid.setOnItemClickListener((parent, view, position, id) -> {
            Intent detailIntent = new Intent(getActivity(),
                    DetailedViewActivity.class);
            detailIntent.putExtra(Consts.EXTRA_ITEM, mItemsAdapter.getItem(position));
            startActivity(detailIntent);
        });
        mItemsGrid.setOnItemLongClickListener((parent, view, position, id) -> {
            showPopUpMenu(view, position);
            return true;
        });

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        // Reload data on fragment resume to show actual database state
        if(getLoaderManager().getLoader(Consts.ITEMS_LOADER_ID) != null){
            getLoaderManager().getLoader(Consts.ITEMS_LOADER_ID).forceLoad();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // Destroy CursorLoader and connection to database to avoid crashes.
        getLoaderManager().destroyLoader(Consts.ITEMS_LOADER_ID);
        mDb.close();
    }

    /**
     * Shows pop-up menu with delete option near grid item at specified.
     *
     * @param v View that pop-up menu uses as anchor.
     * @param position Position of selected item
     */
    private void showPopUpMenu(View v, final int position) {
        PopupMenu itemPopup = new PopupMenu(getActivity(), v);
        itemPopup.inflate(R.menu.menu_saved_item);
        itemPopup.setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.action_delete) {
                showDeleteDialog(position);
                return true;
            }
            return false;
        });

        itemPopup.show();
    }

    /**
     * Shows Alert Dialoge to confirm deleting of item.
     *
     * @param position Position of item to delete
     */
    private void showDeleteDialog(final int position) {
        AlertDialog.Builder deleteDialogBuilder = new AlertDialog.Builder(getActivity());

        deleteDialogBuilder.setTitle(getString(R.string.delete_dialog_title));
        deleteDialogBuilder.setPositiveButton(getString(R.string.delete_dialog_yes), (dialog, which) -> {

            // Delete item from a database and force loader to load actual data
            mDb.deleteItem(mItemsAdapter.getItem(position));
            getLoaderManager().getLoader(Consts.ITEMS_LOADER_ID).forceLoad();
        });

        deleteDialogBuilder.setNegativeButton(getString(R.string.delete_dialog_no), null);
        deleteDialogBuilder.setCancelable(true);
        deleteDialogBuilder.show();

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new MyCursorLoader(getActivity(), mDb);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        mItemsAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    /**
     * Subclass of {@link android.content.CursorLoader} which provides loader associated
     * with application database's implementation.
     */
    static class MyCursorLoader extends CursorLoader {

        Database db;

        public MyCursorLoader(Context context, Database db) {
            super(context);
            this.db = db;
        }

        @Override
        public Cursor loadInBackground() {
            return db.getAllItems();
        }

    }
}
