package xyz.spacefox.etsysearch.app.api.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Class that represents Listing's image object from JSON API response.
 */
public class ItemImage implements Serializable {

    @SerializedName("url_570xN")
    private String mUrl;

    private static final long serialVersionUID = 2L;

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        this.mUrl = url;
    }

    @Override
    public int hashCode() {
        String toComp = mUrl;
        return toComp.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null)
            return false;
        if (getClass() != o.getClass())
            return false;
        ItemImage other = (ItemImage) o;
        if (!mUrl.equals(other.getUrl()))
            return false;
        return true;
    }
}
