package xyz.spacefox.etsysearch.app.api;

import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;
import xyz.spacefox.etsysearch.app.api.model.ApiResponse;
import xyz.spacefox.etsysearch.app.api.model.Category;
import xyz.spacefox.etsysearch.app.api.model.ListingItem;

/**
 * Interface to do API requests using retrofit
 */
public interface ApiService {

    @GET("/taxonomy/categories")
    Observable<ApiResponse<Category>> getCategory();

    @GET("/listings/active?includes=MainImage")
    Observable<ApiResponse<ListingItem>> getItems(@Query("keywords") String keywords,
                     @Query("category") String category, @Query("offset") int offset);

}
