package xyz.spacefox.etsysearch.app.ui;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;
import xyz.spacefox.etsysearch.app.R;
import xyz.spacefox.etsysearch.app.api.model.Category;
import xyz.spacefox.etsysearch.app.utils.Consts;

/**
 * Subclass of {@link android.widget.CursorAdapter} which provides cursor adapter for custom list item and dataset.
 */
public class CategoryCursorAdapter extends CursorAdapter {

    private LayoutInflater mInflater;

    public CategoryCursorAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
        mInflater = (LayoutInflater) context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return mInflater.inflate(R.layout.category_spinner_item, viewGroup, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        // Getting item view elements and setting it's content
        TextView categoryName = (TextView) view.findViewById(R.id.category_name);
        categoryName.setText(cursor.getString(cursor.getColumnIndex(Consts.DB_CATEGORY_COL_NAME)));
    }

    /**
     * Returns an {@link xyz.spacefox.etsysearch.app.api.model.Category} object collected
     * from database at specified position.
     * @param position Position of item to get
     */
    public Category getItem (int position) {
        Cursor cursor = getCursor();
        Category item = new Category();
        if(cursor.moveToPosition(position)) {
            item = new Category();
            item.setCategoryId(cursor.getInt(cursor.getColumnIndex(Consts.DB_COL_ID_PRIMARY)));
            item.setCategoryShortName(cursor.getString(cursor.getColumnIndex(Consts.DB_CATEGORY_COL_NAME)));
            item.setCategoryNameId(cursor.getString(cursor.getColumnIndex(Consts.DB_CATEGORY_COL_NAME_ID)));
        }

        return item;
    }


}
