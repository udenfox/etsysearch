package xyz.spacefox.etsysearch.app.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;
import xyz.spacefox.etsysearch.app.utils.Consts;

/**
 * Singletone class that represents API client using Retrofit.
 * Containt GSON configuration.
 */
public class RestClient {
    private static RestClient sInstance = new RestClient();

    private ApiService mApiService;
    private Gson mGson;
    private RestAdapter mRestAdapter;
    private RequestInterceptor mInterceptor;

    private RestClient() {

        mGson = new GsonBuilder().create();

        // Include API key url encoded param to each API request
        mInterceptor = new RequestInterceptor()
        {
            @Override
            public void intercept(RequestFacade request) {
                request.addQueryParam("api_key", Consts.API_KEY);
            }
        };

        //TODO: Set log level to non-verbose afrer release
        mRestAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Consts.API_URL)
                .setConverter(new GsonConverter(mGson))
                .setRequestInterceptor(mInterceptor)
                .build();

        mApiService = mRestAdapter.create(ApiService.class);
    }

    /** Gives client instance */
    public static RestClient getInstance() {
        return sInstance;
    }

    /** Gives api service to work with */
    public ApiService getApiService()
    {
        return mApiService;
    }

    /** Gives GSON object from Rest client */
    public Gson getGson() {
        return mGson;
    }

}
