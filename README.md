# About app #

The application allows user to search, view and save goods from  [Etsy.com](https://www.etsy.com/). [REST API](https://www.etsy.com/developers/documentation/getting_started/api_basics) used to interact with Etsy. 
Before searching user can choose the category of goods from the list that also loaded from [etsy API](https://www.etsy.com/developers/documentation/getting_started/api_basics).

# Key features #
* Interaction with REST API using Retrofit + GSON
* rxJava (rxAndroid) utulized along with retrolambda
* Items saved by user stored locally in a SQLite database
* Image loading and cache using Picasso
* The Results grid list has Swipe to refresh functionality. New data loaded automatically when user scrolls to the end of the list
* Material design
* Main activity hosts two tabs using ViewPager and TabLayout

[**Download APK**](https://bitbucket.org/udenfox/etsysearch/raw/d312b5d9b74931d3cf3e8004e93782f497da9bd6/app-debug.apk)